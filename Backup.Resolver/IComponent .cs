﻿namespace Backup.Resolver
{
    public interface IComponent 
    {
        /// <summary>
        /// Register underlying types with unity.
        /// </summary>
        void SetUp(IRegisterComponent registerComponent);
    }
}