﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backup.BusinessEntities
{
    public class ClientEntity
    {
        public string IpAddress { get; set; }
        public string MachineName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
