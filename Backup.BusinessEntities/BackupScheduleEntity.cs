﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backup.BusinessEntities
{
    public class BackupScheduleEntity
    {
        public int Id { get; set; }
        public string IpAddress { get; set; }
        public string SourcePath { get; set; }
        public string DestinationPath { get; set; }
        public string ServerUserName { get; set; }
        public string ServerPassword { get; set; }
        public DateTime SchedueTime { get; set; }
        public string ScheduleType { get; set; }
        public bool IsEnabled { get; set; }
        public string Status { get; set; }
    }
}
