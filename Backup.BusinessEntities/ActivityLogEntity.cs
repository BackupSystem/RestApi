﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backup.BusinessEntities
{
    public class ActivityLogEntity
    {
        public int Id { get; set; }
        public string IpAddress { get; set; }
        public string LogData { get; set; }
        public DateTime? TimeStamp { get; set; }
    }
}
