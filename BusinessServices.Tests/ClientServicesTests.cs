﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Backup.BusinessEntities;
using Backup.BusinessServices;
using Backup.DataModel;
using Backup.TestHelper;
using Moq;
using NUnit.Framework;

namespace BusinessServices.Tests
{
    public class ClientServicesTests
    {
        #region Variables
        private IClientServices _clientServices;
        private IUnitOfWork _unitOfWork;
        private List<Client> _clients;
        private GenericRepository<Client> _clientRepository;
        private BackupDbEntities _dbEntities;
        #endregion

        #region Test fixture setup

        /// <summary>
        /// Initial setup for tests
        /// </summary>
        [TestFixtureSetUp]
        public void Setup()
        {
            _clients = SetUpClients();
        }

        #endregion

        private static List<Client> SetUpClients()
        {
            //var id = new int();

            var clients = DataInitializer.GetAllClients();
            //foreach (Client client in clients)
            //{
            //    client.IpAddress = ++id;
            //}

            return clients;
        }

        #region TestFixture TearDown.

        /// <summary>
        /// TestFixture teardown
        /// </summary>
        [TestFixtureTearDown]
        public void DisposeAllObjects()
        {
            _clients = null;
        }

        #endregion

        #region Setup
        /// <summary>
        /// Re-initializes test.
        /// </summary>
        [SetUp]
        public void ReInitializeTest()
        {
            _dbEntities = new Mock<BackupDbEntities>().Object;
            _clientRepository = SetUpClientRepository();
            var unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.SetupGet(s => s.ClientRepository).Returns(_clientRepository);
            _unitOfWork = unitOfWork.Object;
            _clientServices = new ClientServices(_unitOfWork);         
        }

        #endregion


        [Test]
        public void GetAllClientsTest()
        {
            var clients = _clientServices.GetAllClients();
            var clientLists = clients.Select(clientEntity => new Client { IpAddress = clientEntity.IpAddress, UserName = clientEntity.UserName }).ToList();
            var comparer = new ClientComparer();
            CollectionAssert.AreEqual(
            clientLists.OrderBy(client => client, comparer),
            _clients.OrderBy(product => product, comparer), comparer);
        }

        /// <summary>
        /// Service should return null
        /// </summary>
        [Test]
        public void GetAllClientsTestForNull()
        {
            _clients.Clear();
            var products = _clientServices.GetAllClients();
            Assert.Null(products);
            SetUpClients();
        }


        /// <summary>
        /// Service should return client if correct ip is supplied
        /// </summary>
        [Test]
        public void GetClientByRightIpTest()
        {
            var client = _clientServices.GetClientByIpAddress("1.1.1.1");
            if (client != null)
            {
                var config =  new MapperConfiguration(cfg => {
                    cfg.CreateMap<ClientEntity, Client>();
                });

                var mapper = config.CreateMapper();

                var clientModel = mapper.Map<ClientEntity, Client>(client);
                AssertObjects.PropertyValuesAreEquals(clientModel,
                                                      _clients.Find(a => a.IpAddress.Contains("1.1.1.1")));
            }
        }

        /// <summary>
        /// Service should return null
        /// </summary>
        [Test]
        public void GetClientByWrongIpTest()
        {
            var product = _clientServices.GetClientByIpAddress("invalidIp");
            Assert.Null(product);
        }

        /// <summary>
        /// Add new product test
        /// </summary>
        [Test]
        public void AddNewClientTest()
        {
            var newClient = new ClientEntity()
            {               
                MachineName = "Android",               
            };

            var ipAddress = _clients.Last().IpAddress;
            newClient.IpAddress = ipAddress + IPAddress.Parse(ipAddress).GetAddressBytes()[3] + 1;
            _clientServices.CreateClient(newClient);
            var addedClient = new Client() { MachineName = newClient.MachineName, IpAddress = newClient.IpAddress,UserName = newClient.UserName,Password = newClient.Password};
            AssertObjects.PropertyValuesAreEquals(addedClient, _clients.Last());
            Assert.That(newClient.IpAddress, Is.EqualTo(_clients.Last().IpAddress));
        }

        /// <summary>
        /// Tears down each test data
        /// </summary>
        [TearDown]
        public void DisposeTest()
        {
            _clientServices = null;
            _unitOfWork = null;
            _clientRepository = null;
            if (_dbEntities != null)
                _dbEntities.Dispose();
        }

        private GenericRepository<Client> SetUpClientRepository()
        {

            // Initialise repository
            var mockRepo = new Mock<GenericRepository<Client>>(MockBehavior.Default, _dbEntities);

            // Setup mocking behavior
            mockRepo.Setup(p => p.GetAll()).Returns(_clients);

            mockRepo.Setup(p => p.GetByID(It.IsAny<string>()))
            .Returns(new Func<string, Client>(
            ip => _clients.Find(p => p.IpAddress.Equals(ip))));

            mockRepo.Setup(p => p.Insert((It.IsAny<Client>())))
            .Callback(new Action<Client>(newClient =>
            {
                dynamic ipAddress = _clients.Last().IpAddress;            
                dynamic nextipAddress = ipAddress + IPAddress.Parse(ipAddress).GetAddressBytes()[3] + 1;                
                newClient.IpAddress = nextipAddress;
                _clients.Add(newClient);
            }));

            mockRepo.Setup(p => p.Update(It.IsAny<Client>()))
            .Callback(new Action<Client>(client =>
            {
                var oldClient = _clients.Find(a => a.IpAddress == client.IpAddress);
                oldClient = client;
            }));

            mockRepo.Setup(p => p.Delete(It.IsAny<Client>()))
            .Callback(new Action<Client>(prod =>
            {
                var clientToRemove = _clients.Find(a => a.IpAddress == prod.IpAddress);

                if (clientToRemove != null)
                    _clients.Remove(clientToRemove);
            }));

            // Return mock implementation object
            return mockRepo.Object;
        }
    }
}
