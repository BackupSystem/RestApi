﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backup.DataModel;

namespace Backup.TestHelper
{
    public class ClientComparer : IComparer, IComparer<Client>
    {
        public int Compare(object expected, object actual)
        {
            var lhs = expected as Client;
            var rhs = actual as Client;
            if (lhs == null || rhs == null) throw new InvalidOperationException();
            return Compare(lhs, rhs);
        }

        public int Compare(Client expected, Client actual)
        {
            int temp;
            return (temp = expected.IpAddress.CompareTo(actual.IpAddress)) != 0 ? temp : expected.UserName.CompareTo(actual.UserName);
        }
    }
}
