﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backup.DataModel;

namespace Backup.TestHelper
{
    public class DataInitializer
    {
        /// <summary>
        /// Dummy clients
        /// </summary>
        /// <returns></returns>
        public static List<Client> GetAllClients()
        {
            var products = new List<Client>
            {
                new Client()
                {
                    IpAddress = "1.1.1.1",
                    MachineName = "Test1Machine",
                    UserName = "Test1UserName",
                    Password = "Test1Password"
                },
                new Client()
                {
                    IpAddress = "1.1.1.2",
                    MachineName = "Test2Machine",
                    UserName = "Test2UserName",
                    Password = "Test2Password"
                },
                new Client()
                {
                    IpAddress = "1.1.1.3",
                    MachineName = "Test3Machine",
                    UserName = "Test3UserName",
                    Password = "Test3Password"
                },
                new Client()
                {
                    IpAddress = "1.1.1.4",
                    MachineName = "Test4Machine",
                    UserName = "Test4UserName",
                    Password = "Test4Password"
                },
            };
            return products;
        }

        /// <summary>
        /// Dummy tokens
        /// </summary>
        /// <returns></returns>
        public static List<Token> GetAllTokens()
        {
            var tokens = new List<Token>
            {
                new Token()
                {
                    AuthToken = "9f907bdf-f6de-425d-be5b-b4852eb77761",
                    ExpiresOn = DateTime.Now.AddHours(2),
                    IssuedOn = DateTime.Now,
                    UserId = 1
                },
                new Token()
                {
                    AuthToken = "9f907bdf-f6de-425d-be5b-b4852eb77762",
                    ExpiresOn = DateTime.Now.AddHours(1),
                    IssuedOn = DateTime.Now,
                    UserId = 2
                }
            };

            return tokens;
        }

        /// <summary>
        /// Dummy users
        /// </summary>
        /// <returns></returns>
        public static List<User> GetAllUsers()
        {
            var users = new List<User>
            {
                new User()
                {
                    UserName = "John",
                    Password = "Fernandis",
                    UserId = 1
                },
                new User()
                {
                    UserName = "Peter",
                    Password = "England",
                    UserId = 2
                },
                new User()
                {
                    UserName = "Charles",
                    Password = "Georgy",
                    UserId = 3
                }
            };

            return users;
        }

        /// <summary>
        /// Dummy Activity Logs
        /// </summary>
        /// <returns></returns>       
        public static List<ActivityLog> GetAllActivityLogs()
        {
            var activityLogs = new List<ActivityLog>
            {
                new ActivityLog()
                {
                    IpAddress = "1.1.1.1",
                    Id = 1,
                    LogData = "TestLogData1"
                },
                new ActivityLog()
                {
                    IpAddress = "1.1.1.2",
                    Id = 2,
                    LogData = "TestLogData2"
                },
                new ActivityLog()
                {
                    IpAddress = "1.1.1.3",
                    Id = 3,
                    LogData = "TestLogData3"
                },
            };

            return activityLogs;
        }

        /// <summary>
        /// Dummy Backup Schedule
        /// </summary>
        /// <returns></returns>       
        public static List<BackupSchedule> GetAllBackuActivityLogs()
        {
            var activityLogs = new List<BackupSchedule>
            {
                new BackupSchedule()
                {
                    IpAddress = "1.1.1.1",
                    Id = 1,
                    Status = "Queued",
                    DestinationPath = "C:/Backup",
                    IsEnabled = true,
                    SchedueTime = DateTime.Now.AddHours(3),
                    ScheduleType = "Weekly",
                    ServerPassword = "ServerPassword",
                    ServerUserName = "ServerUserName",
                    SourcePath = "D:/TakeBackup",                    
                },
                new BackupSchedule()
                {
                    IpAddress = "1.1.1.2",
                    Id = 1,
                    Status = "Running",
                    DestinationPath = "C:/Backup",
                    IsEnabled = true,
                    SchedueTime = DateTime.Now.AddHours(6),
                    ScheduleType = "Monthly",
                    ServerPassword = "ServerPassword",
                    ServerUserName = "ServerUserName",
                    SourcePath = "D:/TakeBackup",
                },
                new BackupSchedule()
                {
                    IpAddress = "1.1.1.3",
                    Id = 1,
                    Status = "Completed",
                    DestinationPath = "C:/Backup",
                    IsEnabled = true,
                    SchedueTime = DateTime.Now.AddHours(9),
                    ScheduleType = "Daily",
                    ServerPassword = "ServerPassword",
                    ServerUserName = "ServerUserName",
                    SourcePath = "D:/TakeBackup",
                }
            };

            return activityLogs;
        }
    }
}
