﻿using System.Collections.Generic;
using Backup.BusinessEntities;

namespace Backup.BusinessServices
{
    /// <summary>
    /// Activity Logs Service Contract
    /// </summary>
    public interface IActivityLogsServices
    {
        ActivityLogEntity GetActivityLogsByIpAddress(string ipAddress);
        IEnumerable<ActivityLogEntity> GetAllActivityLogs();
        int CreateActivityLog(ActivityLogEntity activityLogEntity);
        bool UpdateActivityLog(string ipAddress, ActivityLogEntity activityLogEntity);
        bool DeleteActivityLog(string ipAddress);
    }
}