﻿using System.Collections.Generic;
using Backup.BusinessEntities;

namespace Backup.BusinessServices
{
    /// <summary>
    /// Client Service Contract
    /// </summary>
    public interface IClientServices
    {
        ClientEntity GetClientByIpAddress(string ipAddress);
        IEnumerable<ClientEntity> GetAllClients();
        int CreateClient(ClientEntity clientEntity);
        bool UpdateClient(string ipAddress, ClientEntity clientEntity);
        bool DeleteClient(string ipAddress);
    }
}