﻿using System.Collections.Generic;
using Backup.BusinessEntities;

namespace Backup.BusinessServices
{
    /// <summary>
    /// Backup Schedule Service Contract
    /// </summary>
    public interface IBackupScheduleServices
    {
        List<BackupScheduleEntity> GetBackupScheduleByIpAddress(string ipAddress);
        IEnumerable<BackupScheduleEntity> GetAllBackupSchedules();
        int CreateBackupSchedule(BackupScheduleEntity backupScheduleEntity);
        bool UpdateBackupSchedule(string ipAddress, BackupScheduleEntity backupScheduleEntity);
        bool DeleteBackupSchedule(string ipAddress);
    }
}