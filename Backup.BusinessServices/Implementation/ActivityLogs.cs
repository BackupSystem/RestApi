﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using AutoMapper;
using Backup.BusinessEntities;
using Backup.DataModel;

namespace Backup.BusinessServices
{
    public class ActivityLogsServices: IActivityLogsServices
    {
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Public constructor.
        /// </summary>
        public ActivityLogsServices(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Fetches Activity logs by ip Address
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public ActivityLogEntity GetActivityLogsByIpAddress(string ipAddress)
        {
            var activityLog = _unitOfWork.ActivityLogRepository.GetByID(ipAddress);
            if (activityLog != null)
            {
                var config = CreateMapperConfiguration();

                var mapper = config.CreateMapper();
                var activityLogModel = mapper.Map<ActivityLog, ActivityLogEntity>(activityLog);
                return activityLogModel;               
            }
            return null;
        }

        private static MapperConfiguration CreateMapperConfiguration()
        {
            return new MapperConfiguration(cfg => {
                                                      cfg.CreateMap<ActivityLog, ActivityLogEntity>();
            });
        }

        /// <summary>
        /// Fetches all the ActivityLogs.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ActivityLogEntity> GetAllActivityLogs()
        {
            var activityLogs = _unitOfWork.ActivityLogRepository.GetAll().ToList();
            if (activityLogs.Any())
            {
                var config = CreateMapperConfiguration();
                var mapper = config.CreateMapper();
                var activityLogModel = mapper.Map<List<ActivityLog>, List<ActivityLogEntity>>(activityLogs);
                return activityLogModel;
            }
            return null;
        }

        /// <summary>
        /// Create Acticity logs
        /// </summary>
        /// <param name="activityLogEntity"></param>
        /// <returns></returns>
        public int CreateActivityLog(ActivityLogEntity activityLogEntity)
        {
            using (var scope = new TransactionScope())
            {
                var activityLog = new ActivityLog
                {
                    IpAddress = activityLogEntity.IpAddress,
                    Id = activityLogEntity.Id,
                    LogData = activityLogEntity.LogData,                    
                };
                _unitOfWork.ActivityLogRepository.Insert(activityLog);
                _unitOfWork.Save();
                scope.Complete();
                
                return activityLog.Id;
            }
        }

        public bool UpdateActivityLog(string ipAddress, ActivityLogEntity activityLogEntity)
        {
            var success = false;
            if (activityLogEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var activityLog = _unitOfWork.ActivityLogRepository.GetByID(ipAddress);

                    if (activityLog != null)
                    {
                        activityLog.Id = activityLogEntity.Id;
                        activityLog.IpAddress = activityLogEntity.IpAddress;
                        activityLog.LogData = activityLogEntity.LogData;                                                          

                        _unitOfWork.ActivityLogRepository.Update(activityLog);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public bool DeleteActivityLog(string ipAddress)
        {
            var success = false;
            if (!string.IsNullOrWhiteSpace(ipAddress))
            {
                using (var scope = new TransactionScope())
                {
                    var client = _unitOfWork.ClientRepository.GetByID(ipAddress);

                    if (client != null && client.ActivityLogs!=null && client.ActivityLogs.Count>0)
                    {
                        var activityLog = client.ActivityLogs.First();

                        _unitOfWork.ActivityLogRepository.Delete(activityLog);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}
