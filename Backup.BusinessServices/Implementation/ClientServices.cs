﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using AutoMapper;
using Backup.BusinessEntities;
using Backup.DataModel;

namespace Backup.BusinessServices
{
    public class ClientServices : IClientServices
    {
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Public constructor.
        /// </summary>
        public ClientServices(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Fetches Client details by ip Address
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public ClientEntity GetClientByIpAddress(string ipAddress)
        {
            var client = _unitOfWork.ClientRepository.GetByID(ipAddress);
            if (client != null)
            {
                var config = CreateMapperConfiguration();
                var mapper = config.CreateMapper();
                var clientModel = mapper.Map<Client, ClientEntity>(client);
                return clientModel;
            }
            return null;
        }

        /// <summary>
        /// Fetches all the Clients.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ClientEntity> GetAllClients()
        {
            var clients = _unitOfWork.ClientRepository.GetAll().ToList();
            if (clients.Any())
            {
                var config = CreateMapperConfiguration();
                var mapper = config.CreateMapper();
                var clientsModel = mapper.Map<List<Client>, List<ClientEntity>>(clients);
                return clientsModel;
            }
            return null;
        }

        private static MapperConfiguration CreateMapperConfiguration()
        {
            return new MapperConfiguration(cfg => {
                                                      cfg.CreateMap<Client, ClientEntity>();
            });
        }

        /// <summary>
        /// Create client
        /// </summary>
        /// <param name="clientEntity"></param>
        /// <returns></returns>
        public int CreateClient(ClientEntity clientEntity)
        {
            using (var scope = new TransactionScope())
            {
                var existingClient = _unitOfWork.ClientRepository.GetByID(clientEntity.IpAddress);

                var client = new Client
                {
                    IpAddress = clientEntity.IpAddress,
                    UserName = clientEntity.UserName,
                    Password = clientEntity.Password,
                    MachineName = clientEntity.MachineName
                };

                if (existingClient != null)
                {
                    _unitOfWork.ClientRepository.Update(client);
                }
                else
                {
                    _unitOfWork.ClientRepository.Insert(client);
                }                           
                
                _unitOfWork.Save();
                scope.Complete();
                //TODO: To make it usable
                return 1;
            }
        }

        public bool UpdateClient(string ipAddress, ClientEntity clientEntity)
        {
            var success = false;
            if (clientEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var client = _unitOfWork.ClientRepository.GetByID(ipAddress);
                    if (client != null)
                    {
                        client.UserName = clientEntity.UserName;
                        client.Password = clientEntity.Password;
                        client.IpAddress = clientEntity.IpAddress;
                        client.MachineName = clientEntity.MachineName;

                        _unitOfWork.ClientRepository.Update(client);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public bool DeleteClient(string ipAddress)
        {
            var success = false;
            if (!string.IsNullOrWhiteSpace(ipAddress))
            {
                using (var scope = new TransactionScope())
                {
                    var client = _unitOfWork.ClientRepository.GetByID(ipAddress);
                    if (client != null)
                    {

                        _unitOfWork.ClientRepository.Delete(client);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}
