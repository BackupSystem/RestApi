﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using AutoMapper;
using Backup.BusinessEntities;
using Backup.DataModel;


namespace Backup.BusinessServices
{
    public class BackupScheduleServices : IBackupScheduleServices
    {
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Public constructor.
        /// </summary>
        public BackupScheduleServices(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Fetches Bacup schedules by ip Address
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public List<BackupScheduleEntity> GetBackupScheduleByIpAddress(string ipAddress)
        {
            List<BackupScheduleEntity> apiBackupScheduleEntities = new List<BackupScheduleEntity>();

            var clients = _unitOfWork.ClientRepository.GetByID(ipAddress);
            if (clients != null && clients.BackupSchedules != null && clients.BackupSchedules.Count > 0)
            {
                var backupSchedules = clients.BackupSchedules.Where(x => x.IpAddress == ipAddress);

                var backupSchedulesEnumerable = backupSchedules as IList<BackupSchedule> ?? backupSchedules.ToList();

                if (backupSchedulesEnumerable.Any())
                {
                    var config = CreateMapperConfiguration();
                    var mapper = config.CreateMapper();

                    apiBackupScheduleEntities.AddRange(backupSchedulesEnumerable.Select(schedule => mapper.Map<BackupSchedule, BackupScheduleEntity>(schedule)));                   
                }
            }

            return null;
        }

        private static MapperConfiguration CreateMapperConfiguration()
        {
            return new MapperConfiguration(cfg => {
                                                      cfg.CreateMap<BackupSchedule, BackupScheduleEntity>();
            });
        }

        /// <summary>
        /// Fetches all the BackupSchedules.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<BackupScheduleEntity> GetAllBackupSchedules()
        {
            var backupSchedules = _unitOfWork.BackupScheduleRepository.GetAll().ToList();
            if (backupSchedules.Any())
            {
                var config = CreateMapperConfiguration();
                var mapper = config.CreateMapper();
                var backupSchedulesModel = mapper.Map<List<BackupSchedule>, List<BackupScheduleEntity>>(backupSchedules);
                return backupSchedulesModel;
            }
            return null;
        }

        /// <summary>
        /// Create a backup schedule
        /// </summary>
        /// <param name="backupScheduleEntity"></param>
        /// <returns></returns>
        public int CreateBackupSchedule(BackupScheduleEntity backupScheduleEntity)
        {
            using (var scope = new TransactionScope())
            {
                var backupSchedule = new BackupSchedule
                {
                    IpAddress = backupScheduleEntity.IpAddress,
                    Id = backupScheduleEntity.Id,
                    DestinationPath = backupScheduleEntity.DestinationPath,
                    SchedueTime = backupScheduleEntity.SchedueTime,
                    ScheduleType = backupScheduleEntity.ScheduleType,
                    Status =  backupScheduleEntity.Status,
                    ServerPassword = backupScheduleEntity.ServerPassword,
                    ServerUserName = backupScheduleEntity.ServerUserName,
                    SourcePath = backupScheduleEntity.SourcePath,                   
                };
                _unitOfWork.BackupScheduleRepository.Insert(backupSchedule);
                _unitOfWork.Save();
                scope.Complete();

                return backupSchedule.Id;
            }
        }

        public bool UpdateBackupSchedule(string ipAddress, BackupScheduleEntity backupScheduleEntity)
        {
            var success = false;
            if (backupScheduleEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var client = _unitOfWork.ClientRepository.GetByID(ipAddress);

                    if (client != null && client.BackupSchedules != null && client.BackupSchedules.Count > 0)
                    {
                        var backupSchedule = client.BackupSchedules.First();

                        backupSchedule.DestinationPath = backupScheduleEntity.DestinationPath;
                        backupSchedule.SchedueTime = backupScheduleEntity.SchedueTime;
                        backupSchedule.ScheduleType = backupScheduleEntity.ScheduleType;
                        backupSchedule.ServerPassword = backupScheduleEntity.ServerPassword;
                        backupSchedule.ServerUserName = backupScheduleEntity.ServerUserName;
                        backupSchedule.SourcePath = backupScheduleEntity.SourcePath;
                        backupSchedule.IsEnabled = backupScheduleEntity.IsEnabled;
                        backupSchedule.Status = backupScheduleEntity.Status;

                        _unitOfWork.BackupScheduleRepository.Update(backupSchedule);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public bool DeleteBackupSchedule(string ipAddress)
        {
            var success = false;
            if (!string.IsNullOrWhiteSpace(ipAddress))
            {
                using (var scope = new TransactionScope())
                {
                    var client = _unitOfWork.ClientRepository.GetByID(ipAddress);
                    
                    if (client != null && client.BackupSchedules!=null && client.BackupSchedules.Count>0)
                    {
                        var backupSchedule = client.BackupSchedules.First();

                        _unitOfWork.BackupScheduleRepository.Delete(backupSchedule);

                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}
