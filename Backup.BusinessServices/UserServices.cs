﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backup.DataModel;

namespace Backup.BusinessServices
{
    public class UserServices : IUserServices
    {
        private readonly UnitOfWork _unitOfWork;

        public UserServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Offers services for user specific operations
        /// </summary>
        public int Authenticate(string userName, string password)
        {
            var user = _unitOfWork.UserRepository.Get(u => string.Equals(u.UserName, userName, StringComparison.CurrentCultureIgnoreCase) && u.Password == password);

            if (user != null)
            {
                return 1;
            }

            //if (user != null && user.UserId > 0)
            //{
            //    return user.UserId;
            //}
            return 0;
        }
    }
}
