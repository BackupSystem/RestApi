﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backup.DataModel;
using Backup.Resolver;

namespace Backup.BusinessServices
{
    [Export(typeof(IComponent))]
    public class DependencyResolver : IComponent
    {
        public void SetUp(IRegisterComponent registerComponent)
        {
            registerComponent.RegisterType<IClientServices, ClientServices>();
            registerComponent.RegisterType<IBackupScheduleServices, BackupScheduleServices>();
            registerComponent.RegisterType<IActivityLogsServices, ActivityLogsServices>();
            registerComponent.RegisterType<IUserServices, UserServices>();
            registerComponent.RegisterType<ITokenServices, TokenServices>();
            registerComponent.RegisterType<IUnitOfWork, UnitOfWork>();
        }
    }
}
