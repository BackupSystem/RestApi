﻿namespace Backup.BusinessServices
{
    public interface IUserServices
    {
        int Authenticate(string userName, string password);
    }
}