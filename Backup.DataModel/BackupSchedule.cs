//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Backup.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class BackupSchedule
    {
        public int Id { get; set; }
        public string IpAddress { get; set; }
        public string SourcePath { get; set; }
        public string DestinationPath { get; set; }
        public string ServerUserName { get; set; }
        public string ServerPassword { get; set; }
        public System.DateTime SchedueTime { get; set; }
        public string ScheduleType { get; set; }
        public bool IsEnabled { get; set; }
        public string Status { get; set; }
    
        public virtual Client Client { get; set; }
    }
}
