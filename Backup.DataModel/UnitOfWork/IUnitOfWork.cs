﻿namespace Backup.DataModel
{
    public interface IUnitOfWork
    {

        GenericRepository<Client> ClientRepository { get; }
        GenericRepository<BackupSchedule> BackupScheduleRepository { get; }
        GenericRepository<ActivityLog> ActivityLogRepository { get; }
        GenericRepository<User> UserRepository { get; }
        GenericRepository<Token> TokenRepository { get; }

        /// <summary>
        /// Save method.
        /// </summary>
        void Save();
    }
}