﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backup.DataModel;

namespace Backup.DataModel
{
    /// <summary>
    /// Unit of Work class responsible for DB transactions
    /// </summary>
    public class UnitOfWork : IDisposable,IUnitOfWork
    {
        #region Private member variables...

        private readonly BackupDbEntities _context = null;
        private GenericRepository<User> _userRepository;
        private GenericRepository<Client> _clinetRepository;        
        private GenericRepository<Token> _tokenRepository;
        private GenericRepository<BackupSchedule> _backupScheduleRepository;
        private GenericRepository<ActivityLog> _activityLogRepository;
        #endregion

        public UnitOfWork()
        {
            _context = new BackupDbEntities();
        }

        #region Public Repository Creation properties...

        /// <summary>
        /// Get/Set Property for Backup Schedule repository.
        /// </summary>
        public GenericRepository<BackupSchedule> BackupScheduleRepository
        {
            get
            {
                if (this._backupScheduleRepository == null)
                    this._backupScheduleRepository = new GenericRepository<BackupSchedule>(_context);
                return _backupScheduleRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for Activity Log repository.
        /// </summary>
        public GenericRepository<ActivityLog> ActivityLogRepository
        {
            get
            {
                if (this._activityLogRepository == null)
                    this._activityLogRepository = new GenericRepository<ActivityLog>(_context);
                return _activityLogRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for product repository.
        /// </summary>
        public GenericRepository<Client> ClientRepository
        {
            get
            {
                if (this._clinetRepository == null)
                    this._clinetRepository = new GenericRepository<Client>(_context);
                return _clinetRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for user repository.
        /// </summary>
        public GenericRepository<User> UserRepository
        {
            get
            {
                if (this._userRepository == null)
                    this._userRepository = new GenericRepository<User>(_context);
                return _userRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for token repository.
        /// </summary>
        public GenericRepository<Token> TokenRepository
        {
            get
            {
                if (this._tokenRepository == null)
                    this._tokenRepository = new GenericRepository<Token>(_context);
                return _tokenRepository;
            }
        }
        #endregion

        #region Public member methods...
        /// <summary>
        /// Save method.
        /// </summary>
        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                var outputLines = new List<string>();
                foreach (var eve in e.EntityValidationErrors)
                {
                    outputLines.Add(string.Format(
                        "{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:",
                        DateTime.Now,
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName,
                            ve.ErrorMessage));
                    }
                }
                System.IO.File.AppendAllLines(@"C:\errors.txt", outputLines);

                throw e;
            }
            catch (Exception e)
            {

            }

        }

        #endregion

        #region Implementing IDiosposable...

        #region private dispose variable declaration...
        private bool _disposed = false;
        #endregion

        /// <summary>
        /// Protected Virtual Dispose method
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
