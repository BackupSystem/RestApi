using System.Web.Http;
using Backup.BusinessServices;
using Backup.Resolver;
using Microsoft.Practices.Unity;
using Unity.Mvc3;
using DependencyResolver = System.Web.Mvc.DependencyResolver;

namespace BackupRestWebApi
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            System.Web.Mvc.DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            // register dependency resolver for WebAPI RC
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();        
            //container.RegisterType<IClientServices, ClientServices>().RegisterType<UnitOfWork>(new HierarchicalLifetimeManager());
            //container.RegisterType<IBackupScheduleServices, BackupScheduleServices>().RegisterType<UnitOfWork>(new HierarchicalLifetimeManager());
            //container.RegisterType<IActivityLogsServices, ActivityLogsServices>().RegisterType<UnitOfWork>(new HierarchicalLifetimeManager());

            RegisterTypes(container);

            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {
            //Component initialization via MEF
            ComponentLoader.LoadContainer(container, ".\\bin", "BackupRestWebApi.dll");
            ComponentLoader.LoadContainer(container, ".\\bin", "Backup.BusinessServices.dll");

        }
    }
}