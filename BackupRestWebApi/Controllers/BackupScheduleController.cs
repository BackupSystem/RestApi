﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;
using Backup.BusinessEntities;
using Backup.BusinessServices;
using BackupRestWebApi.ActionFilters;
using BackupRestWebApi.Error_Helper;

namespace BackupRestWebApi.Controllers
{
    [LoggingFilter]
    public class BackupScheduleController : ApiController
    {
        private readonly IBackupScheduleServices _backupScheduleServices;

        public BackupScheduleController(IBackupScheduleServices backupScheduleServices)
        {
            _backupScheduleServices = backupScheduleServices;
        }

        // GET api/<controller>
        [LoggingFilter]
        [AuthorizationRequired]
        public HttpResponseMessage Get()
        {
            var backupSchedules = _backupScheduleServices.GetAllBackupSchedules();

            if (backupSchedules != null)
            {
                var backupScheduleEntities = backupSchedules as List<BackupScheduleEntity> ?? backupSchedules.ToList();

                if (backupScheduleEntities.Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, backupScheduleEntities);
                }
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Backup Schedules not found");
        }

        // GET api/<controller>/5
        //[GET("backupschedule/{ipAddress?}")]
        [LoggingFilter]
        public HttpResponseMessage Get(string ipAddress)
        {
            if (!string.IsNullOrWhiteSpace(ipAddress))
            {
                var backupSchedules = _backupScheduleServices.GetBackupScheduleByIpAddress(ipAddress);

                if (backupSchedules != null && backupSchedules.Count>0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, backupSchedules);
                }

                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Backup Schedules not found");
            }
            throw new ApiException() { ErrorCode = (int)HttpStatusCode.BadRequest, ErrorDescription = "Bad Request..." };
        }

        // POST api/<controller>
        [LoggingFilter]
        [AuthorizationRequired]
        public int Post([FromBody]BackupScheduleEntity backupScheduleEntity)
        {
            return _backupScheduleServices.CreateBackupSchedule(backupScheduleEntity);
        }

        // PUT api/<controller>/5
        [LoggingFilter]
        [AuthorizationRequired]
        public bool Put(string ipAddress, [FromBody]BackupScheduleEntity backupScheduleEntity)
        {
            if (!string.IsNullOrWhiteSpace(ipAddress))
            {
                return _backupScheduleServices.UpdateBackupSchedule(ipAddress, backupScheduleEntity);
            }

            return false;
        }

        // DELETE api/<controller>/5
        [LoggingFilter]
        [AuthorizationRequired]
        public bool Delete(string ipAddress)
        {
            if (!string.IsNullOrWhiteSpace(ipAddress))
            {
                bool isSuccess = _backupScheduleServices.DeleteBackupSchedule(ipAddress);

                if (isSuccess)
                {
                    return true;
                }

                throw new ApiDataException(1002, "Backup schedule is already deleted or not exist in system.", HttpStatusCode.NoContent);
            }

            throw new ApiException() { ErrorCode = (int)HttpStatusCode.BadRequest, ErrorDescription = "Bad Request..." };
        }
    }
}