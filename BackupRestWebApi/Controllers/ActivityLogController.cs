﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;
using Backup.BusinessEntities;
using Backup.BusinessServices;
using BackupRestWebApi.ActionFilters;
using BackupRestWebApi.Error_Helper;

namespace BackupRestWebApi.Controllers
{
    [LoggingFilter]
    public class ActivityLogController : ApiController
    {
        private readonly IActivityLogsServices _activityLogsServices;

        public ActivityLogController(IActivityLogsServices activityLogsServices)
        {
            _activityLogsServices = activityLogsServices;
        }

        // GET api/<controller>
        //[GET("api/activitylog/getactivitylogs")]
        [LoggingFilter]
        [AuthorizationRequired]
        public HttpResponseMessage Get()
        {
            var activityLogs = _activityLogsServices.GetAllActivityLogs();

            if (activityLogs != null)
            {
                var activityLogsEntities = activityLogs as List<ActivityLogEntity> ?? activityLogs.ToList();

                if (activityLogsEntities.Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, activityLogsEntities);
                }
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Activity Logs not found");
        }

        // GET api/<controller>/5
        //[GET("api/activitylog/getActivitylog/{ipAddress?}")]
        [LoggingFilter]
        [AuthorizationRequired]
        public HttpResponseMessage Get(string ipAddress)
        {
            if (!string.IsNullOrWhiteSpace(ipAddress))
            {
                var activityLog = _activityLogsServices.GetActivityLogsByIpAddress(ipAddress);

                if (activityLog != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, activityLog);
                }
                throw new ApiDataException(1001, "No Activity Logs found for this ip address.", HttpStatusCode.NotFound);
            }
            throw new ApiException() { ErrorCode = (int)HttpStatusCode.BadRequest, ErrorDescription = "Bad Request..." };
        }

        // POST api/<controller>
        //[POST("api/activitylog/createActivityLog")]
        [LoggingFilter]
        //[AuthorizationRequired]
        public int Post([FromBody]ActivityLogEntity activityLogEntity)
        {
            return _activityLogsServices.CreateActivityLog(activityLogEntity);
        }

        // PUT api/<controller>/5
        //[PUT("api/activitylog/updateActivityLog")]
        [LoggingFilter]
        [AuthorizationRequired]
        public bool Put(string ipAddress, [FromBody]ActivityLogEntity activityLogEntity)
        {
            if (!string.IsNullOrWhiteSpace(ipAddress))
            {
                return _activityLogsServices.UpdateActivityLog(ipAddress, activityLogEntity);
            }

            return false;
        }

        // DELETE api/<controller>/5
        //[DELETE("api/activitylog/deleteActivityLog")]
        [LoggingFilter]
        [AuthorizationRequired]
        public bool Delete(string ipAddress)
        {
            if (!string.IsNullOrWhiteSpace(ipAddress))
            {
               bool isSuccess =  _activityLogsServices.DeleteActivityLog(ipAddress);

               if (isSuccess)
               {
                   return true;
               }
                throw new ApiDataException(1002, "Activity log is already deleted or not exist in system.", HttpStatusCode.NoContent);
            }

            throw new ApiException() { ErrorCode = (int)HttpStatusCode.BadRequest, ErrorDescription = "Bad Request..." };
        }
    }
}