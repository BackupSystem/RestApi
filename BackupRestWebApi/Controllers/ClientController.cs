﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Backup.BusinessEntities;
using Backup.BusinessServices;
using AttributeRouting;
using AttributeRouting.Web.Http;
using BackupRestWebApi.ActionFilters;
using BackupRestWebApi.Error_Helper;
using BackupRestWebApi.Filters;

namespace BackupRestWebApi.Controllers
{
    [LoggingFilter]    
    public class ClientController : ApiController
    {
        private readonly IClientServices _clientServices;

        /// <summary>
        /// Public constructor to initialize client instance
        /// </summary>
        public ClientController(IClientServices clientServices)
        {
            _clientServices = clientServices;
        }

        // GET api/<controller>
        // [ApiAuthenticationFilter(true)]
        [LoggingFilter]
        [AuthorizationRequired]
        public HttpResponseMessage Get()
        {
            var clients = _clientServices.GetAllClients();

            if (clients != null)
            {
                var clientEntities = clients as List<ClientEntity> ?? clients.ToList();

                if (clientEntities.Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, clientEntities);
                }
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Clients not found");
        }

        // GET api/<controller>/5
        //[GET("client/{ipAddress?}")]
        [LoggingFilter]
        [AuthorizationRequired]
        public HttpResponseMessage Get(string ipAddress)
        {
            if (!string.IsNullOrWhiteSpace(ipAddress))
            {
                var client = _clientServices.GetClientByIpAddress(ipAddress);

                if (client != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, client);
                }
                throw new ApiDataException(1001, "No clinet found for this ip address.", HttpStatusCode.NotFound);
                //return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No client found for this ipAddress");
            }

            throw new ApiException() { ErrorCode = (int)HttpStatusCode.BadRequest, ErrorDescription = "Bad Request..." };
        }

        // POST api/<controller>
        [LoggingFilter]
        public int Post([FromBody]ClientEntity clientEntity)
        {
            if (clientEntity != null && !string.IsNullOrWhiteSpace(clientEntity.IpAddress))
            {
                var client = _clientServices.GetClientByIpAddress(clientEntity.IpAddress);

                if (client == null)
                {
                    return _clientServices.CreateClient(clientEntity);
                }                
            }

            return 1;
        }

        // PUT api/<controller>/5
        [LoggingFilter]
        [AuthorizationRequired]
        public bool Put(string ipAddress, [FromBody]ClientEntity clientEntity)
        {
            if (!string.IsNullOrWhiteSpace(ipAddress))
            {
                return _clientServices.UpdateClient(ipAddress, clientEntity);
            }

            return false;
        }

        // DELETE api/<controller>/5
        [LoggingFilter]
        [AuthorizationRequired]
        public bool Delete(string ipAddress)
        {
            if (!string.IsNullOrWhiteSpace(ipAddress))
            {
                bool isSuccess = _clientServices.DeleteClient(ipAddress);

                if (isSuccess)
                {
                    return true;
                }
                throw new ApiDataException(1002, "Client is already deleted or not exist in system.", HttpStatusCode.NoContent);
            }

            throw new ApiException() { ErrorCode = (int)HttpStatusCode.BadRequest, ErrorDescription = "Bad Request..." };
        }
    }
}