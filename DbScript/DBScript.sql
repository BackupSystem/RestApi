USE [master]
GO
/****** Object:  Database [BackupSystem]    Script Date: 01/29/2017 17:30:10 ******/
CREATE DATABASE [BackupSystem] ON  PRIMARY 
( NAME = N'BackupSystem', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\BackupSystem.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BackupSystem_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\BackupSystem_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BackupSystem] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BackupSystem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BackupSystem] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [BackupSystem] SET ANSI_NULLS OFF
GO
ALTER DATABASE [BackupSystem] SET ANSI_PADDING OFF
GO
ALTER DATABASE [BackupSystem] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [BackupSystem] SET ARITHABORT OFF
GO
ALTER DATABASE [BackupSystem] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [BackupSystem] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [BackupSystem] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [BackupSystem] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [BackupSystem] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [BackupSystem] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [BackupSystem] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [BackupSystem] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [BackupSystem] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [BackupSystem] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [BackupSystem] SET  DISABLE_BROKER
GO
ALTER DATABASE [BackupSystem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [BackupSystem] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [BackupSystem] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [BackupSystem] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [BackupSystem] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [BackupSystem] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [BackupSystem] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [BackupSystem] SET  READ_WRITE
GO
ALTER DATABASE [BackupSystem] SET RECOVERY SIMPLE
GO
ALTER DATABASE [BackupSystem] SET  MULTI_USER
GO
ALTER DATABASE [BackupSystem] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [BackupSystem] SET DB_CHAINING OFF
GO
USE [BackupSystem]
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 01/29/2017 17:30:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[IpAddress] [nvarchar](20) NOT NULL,
	[MachineName] [nvarchar](20) NULL,
	[UserName] [nvarchar](20) NOT NULL,
	[Password] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Clients1] PRIMARY KEY CLUSTERED 
(
	[IpAddress] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Clients] ([IpAddress], [MachineName], [UserName], [Password]) VALUES (N'1.1.1.1', N'Testmachine', N'abhishek', N'abhishek')
/****** Object:  Table [dbo].[Users]    Script Date: 01/29/2017 17:30:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](20) NOT NULL,
	[Password] [nvarchar](20) NOT NULL,
	[UserRole] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_User1_1] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Users] ON
INSERT [dbo].[Users] ([UserId], [UserName], [Password], [UserRole]) VALUES (1, N'abhishek', N'abhishek', N'Admin')
SET IDENTITY_INSERT [dbo].[Users] OFF
/****** Object:  Table [dbo].[Tokens]    Script Date: 01/29/2017 17:30:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tokens](
	[TokenId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[UserName] [nvarchar](20) NULL,
	[AuthToken] [nvarchar](250) NOT NULL,
	[IssuedOn] [datetime] NOT NULL,
	[ExpiresOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Tokens1] PRIMARY KEY CLUSTERED 
(
	[TokenId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Tokens] ON
INSERT [dbo].[Tokens] ([TokenId], [UserId], [UserName], [AuthToken], [IssuedOn], [ExpiresOn]) VALUES (1, 1, NULL, N'7f8e9e64-597f-41dc-a7dc-b8ec685b805d', CAST(0x0000A70900E63833 AS DateTime), CAST(0x0000A709011389C3 AS DateTime))
SET IDENTITY_INSERT [dbo].[Tokens] OFF
/****** Object:  Table [dbo].[BackupSchedule]    Script Date: 01/29/2017 17:30:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BackupSchedule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IpAddress] [nvarchar](20) NOT NULL,
	[SourcePath] [nvarchar](500) NOT NULL,
	[DestinationPath] [nvarchar](500) NOT NULL,
	[ServerUserName] [nvarchar](20) NOT NULL,
	[ServerPassword] [nvarchar](30) NOT NULL,
	[SchedueTime] [datetime] NOT NULL,
	[ScheduleType] [nvarchar](10) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[Status] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_BackupSchedule1_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[BackupSchedule] ON
INSERT [dbo].[BackupSchedule] ([Id], [IpAddress], [SourcePath], [DestinationPath], [ServerUserName], [ServerPassword], [SchedueTime], [ScheduleType], [IsEnabled], [Status]) VALUES (1, N'1.1.1.1', N'C:/SourceBackup', N'C:/DestBackup', N'TestUserName', N'TestPassword', CAST(0x0000A709006C41F0 AS DateTime), N'Daily', 1, N'Open')
SET IDENTITY_INSERT [dbo].[BackupSchedule] OFF
/****** Object:  Table [dbo].[ActivityLogs]    Script Date: 01/29/2017 17:30:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IpAddress] [nvarchar](20) NOT NULL,
	[LogData] [ntext] NOT NULL,
	[DateInserted] [datetime] NOT NULL,
 CONSTRAINT [PK_ActivityLogs1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ActivityLogs] ON
INSERT [dbo].[ActivityLogs] ([Id], [IpAddress], [LogData], [DateInserted]) VALUES (1, N'1.1.1.1', N'TestLog', CAST(0x0000A70900C8D216 AS DateTime))
SET IDENTITY_INSERT [dbo].[ActivityLogs] OFF
/****** Object:  Default [DF__ActivityL__DateI__4D94879B]    Script Date: 01/29/2017 17:30:11 ******/
ALTER TABLE [dbo].[ActivityLogs] ADD  DEFAULT (getdate()) FOR [DateInserted]
GO
/****** Object:  ForeignKey [FK_Tokens1_Users1]    Script Date: 01/29/2017 17:30:11 ******/
ALTER TABLE [dbo].[Tokens]  WITH CHECK ADD  CONSTRAINT [FK_Tokens1_Users1] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Tokens] CHECK CONSTRAINT [FK_Tokens1_Users1]
GO
/****** Object:  ForeignKey [FK_BackupSchedule1_Clients1]    Script Date: 01/29/2017 17:30:11 ******/
ALTER TABLE [dbo].[BackupSchedule]  WITH CHECK ADD  CONSTRAINT [FK_BackupSchedule1_Clients1] FOREIGN KEY([IpAddress])
REFERENCES [dbo].[Clients] ([IpAddress])
GO
ALTER TABLE [dbo].[BackupSchedule] CHECK CONSTRAINT [FK_BackupSchedule1_Clients1]
GO
/****** Object:  ForeignKey [FK_ActivityLogs1_Clients]    Script Date: 01/29/2017 17:30:11 ******/
ALTER TABLE [dbo].[ActivityLogs]  WITH CHECK ADD  CONSTRAINT [FK_ActivityLogs1_Clients] FOREIGN KEY([IpAddress])
REFERENCES [dbo].[Clients] ([IpAddress])
GO
ALTER TABLE [dbo].[ActivityLogs] CHECK CONSTRAINT [FK_ActivityLogs1_Clients]
GO
